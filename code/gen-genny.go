//go:build ignore

// START1 OMIT
package slices

func AppendString(slice []string, elems ...string) []string {
	return append(slice, elems...)
}

func AppendInt(slice []int, elems ...int) []int {
	return append(slice, elems...)
}

// END1 OMIT
