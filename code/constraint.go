package main

import "fmt"

// START1 OMIT

type numbers interface {
	int | int32 | int64 // type union making type set // HL
}

func Max[T numbers](numbers ...T) T {
	var max T
	for i, n := range numbers {
		if n > max || i == 0 {
			max = n
		}
	}
	return max
}

func main() {
	max := Max(1, 2, 3, 4, 5)
	fmt.Println("max:", max)
}

// END1 OMIT
