package main

import "fmt"

func Append[T any](slice []T, elems ...T) []T { // HL
	fmt.Printf("len: %d, cap: %d => ", len(slice), cap(slice))
	out := append(slice, elems...)
	fmt.Printf("len: %d, cap: %d\n", len(out), cap(out))
	return out
}

func main() {
	var sl []int
	sl = Append[int](sl, 1) // HL
	sl = Append(sl, 2)      // HL
	sl = Append(sl, 3)
	sl = Append(sl, 4)
	sl = Append(sl, 5)
	_ = sl
}
