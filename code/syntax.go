// Define the function f with a type parameter T
func f[T any](t T) { // HL
	// ...
}

// Call the function with a concrete type
f[int](1) // HL

type S[T any] struct { v T } // HL

func (S[T]) method() { // HL
	// ...
}
