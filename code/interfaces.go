package main

import (
	"bytes"
	"io"
	"os"
	"strings"
)

var fileName = func() string {
	f, _ := os.CreateTemp(os.TempDir(), "")
	_, _ = f.WriteString("*os.File\n")
	_ = f.Close()
	return f.Name()
}()

// START1 OMIT
func main() {
	sr := strings.NewReader("*strings.Reader\n")
	_, _ = io.Copy(os.Stdout, sr)

	br := bytes.NewReader([]byte{'*', 'b', 'y', 't', 'e', 's', '.', 'R', 'e', 'a', 'd', 'e', 'r', '\n'})
	_, _ = io.Copy(os.Stdout, br)

	f, _ := os.Open(fileName)
	_, _ = io.Copy(os.Stdout, f)
}

// END1 OMIT
