package main

// START1 OMIT
import (
	"fmt"  // OMIT
	"time" // OMIT
	// OMIT
	"golang.org/x/exp/constraints" // HL
)

// Ordered is a constraint that permits any ordered type: any type
// that supports the operators < <= >= >.
// If future releases of Go add new ordered types,
// this constraint will be modified to include them.
//
//type Ordered interface {
//	Integer | Float | ~string
//}

func Max[T constraints.Ordered](numbers ...T) T { // HL
	var max T
	for i, n := range numbers {
		if n > max || i == 0 {
			max = n
		}
	}
	return max
}

// END1 OMIT

func main() {
	max := Max(time.Minute, time.Second, time.Hour)
	fmt.Println("max:", max)
}
