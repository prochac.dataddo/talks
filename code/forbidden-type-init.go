package main

func New[T any]() T {
	var t T // HL
	// disallowed, what if T is `int`
	t = T{}     // HL
	t = *new(T) // HL
	return t
}
