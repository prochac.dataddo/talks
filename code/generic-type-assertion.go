package main

// START1 OMIT
type A struct{}
type B struct{}
type AorB interface{ A | B }

func DoSomething[T AorB](v T) {
	switch any(v).(type) {
	case A: // do A thing
	case B: // do B thing
	}
}

// END1 OMIT
