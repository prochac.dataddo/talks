package main

import (
	"time"

	"golang.org/x/exp/constraints"
)

// START1 OMIT
type IntegerStringer interface {
	constraints.Integer
	String() string
}

// This is not ok, because interface contains type set. It can be used only as
// type constrain.
var _ IntegerStringer = time.Duration(0) // HL

// END1 OMIT

// START2 OMIT
type IntOrFloat interface {
	int | float64
}
type S struct {
	num IntOrFloat // HL
}

// How would compiler know the type?
var _ = S{num: 0}

// END2 OMIT
