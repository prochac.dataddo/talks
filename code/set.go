package main

import "fmt"

// START1 OMIT

type Set[T comparable] map[T]struct{} // HL

func (s *Set[T]) Add(elems ...T) { // HL
	if (*s) == nil {
		*s = make(map[T]struct{}, len(elems))
	}
	for _, el := range elems {
		(*s)[el] = struct{}{}
	}
}

func (s Set[T]) Contains(el T) bool { // HL
	_, ok := s[el]
	return ok
}

func main() {
	var s Set[int]
	s.Add(1)
	fmt.Println(s.Contains(1))
	fmt.Println(s.Contains(2))
}

// END1 OMIT
