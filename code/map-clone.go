//go:build ignore

// START1 OMIT
package maps // import "golang.org/x/exp/maps"

// Clone returns a copy of m.  This is a shallow clone:
// the new keys and values are set using ordinary assignment.
func Clone[M ~map[K]V, K comparable, V any](m M) M { // HL
	r := make(M, len(m))
	for k, v := range m {
		r[k] = v
	}
	return r
}

// END1 OMIT
