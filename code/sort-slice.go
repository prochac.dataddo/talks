//go:build ignore

package sort

// Slice sorts the slice x given the provided less function.
// It panics if x is not a slice.
// ...
func Slice(x interface{}, less func(i, j int) bool) { // HL
	rv := reflectValueOf(x)   // HL
	swap := reflectSwapper(x) // HL
	length := rv.Len()
	quickSort_func(lessSwap{less, swap}, 0, length, maxDepth(length))
}
