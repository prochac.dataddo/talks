package main

import "fmt"

func Append(slice []interface{}, elems ...interface{}) []interface{} { // HL
	fmt.Printf("len: %d, cap: %d => ", len(slice), cap(slice))
	out := append(slice, elems...)
	fmt.Printf("len: %d, cap: %d\n", len(out), cap(out))
	return out
}

func main() {
	var sl []int
	sl = Append(sl, 1)
	_ = sl
}
