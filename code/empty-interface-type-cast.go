package main

import (
	"container/list"
	"fmt"
)

func main() {
	l := list.New()
	l.PushBack(1)
	l.PushBack(2)
	l.PushBack(3)

	var sum int
	for e := l.Front(); e != nil; e = e.Next() {
		// We need to cast type from empty interface
		sum += e.Value.(int) // HL
	}
	fmt.Println(sum)
}
