package main

import (
	"fmt"
	"time"
)

// START1 OMIT
type numbers interface {
	~int | ~int32 | ~int64 // HL
}

func Max[T numbers](numbers ...T) (max T) {
	for i, n := range numbers {
		if n > max || i == 0 {
			max = n
		}
	}
	return max
}

func main() {
	max := Max(time.Minute, time.Second, time.Hour) // HL
	fmt.Println("max:", max)
}

// END1 OMIT
