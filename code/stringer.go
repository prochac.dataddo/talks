package main

import (
	"fmt"
	"time"
)

// START1 OMIT

type Stringer interface {
	String() string
}

func PrintStringer[S Stringer]() { // HL
	var s S // HL
	fmt.Printf("This guy stirngs: %T\n", s)
}

func main() {
	PrintStringer[time.Time]()
	PrintStringer[time.Duration]()
}

// END1 OMIT
