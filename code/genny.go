//go:build ignore

// START1 OMIT
package slices

import "github.com/cheekybits/genny/generic"

type Something generic.Type

//go:generate genny -in=$GOFILE -out=gen-$GOFILE gen "Something=string,int" // HL

func AppendSomething(slice []Something, elems ...Something) []Something {
	return append(slice, elems...)
}

// END1 OMIT
