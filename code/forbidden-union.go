package main

// START1 OMIT
type InterfaceWithMethod interface {
	Method()
}

// ForbiddenUnion makes type union with interface containing method.
type ForbiddenUnion interface {
	InterfaceWithMethod | any // HL
}

// END1 OMIT
