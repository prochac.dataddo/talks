//go:build ignore

// START1 OMIT

package builtin

// any is an alias for interface{} and is equivalent to interface{} in all ways.
type any = interface{} // HL

// END1 OMIT
